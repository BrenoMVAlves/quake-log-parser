# ==================================================================================
# Project: Quake Game Log Parser and Reporter
# Author: Breno Alves
# Date: 2024-03-11
# ==================================================================================

import pytest
import src.report as _report
from src.game import Game
import os
import json
from src.log_parser import LogParser

CWD = os.path.dirname(__file__)

sample_log_file_path =  os.path.join(CWD, r"sample_data\game1_game2.log")

@pytest.fixture
def games():
    parser = LogParser(sample_log_file_path)
    return parser.parse()



def test_group_report_generator_with_mocked_games(mocker):
    # Mock two Game instances
    mock_game_1 = mocker.Mock(spec=Game)
    mock_game_2 = mocker.Mock(spec=Game)

    # Set up mock attributes and return values for the mocked games
    mock_game_1.get_name.return_value = "Game 1"
    mock_game_1.total_kills = 10
    mock_game_1.players = {1: mocker.Mock(get_formatted_name=lambda: "Player 1", name_history=["Player 1"])}
    mock_game_1.player_kills = {1: 5}
    mock_game_1.player_self_kills = {1: 1}

    mock_game_2.get_name.return_value = "Game 2"
    mock_game_2.total_kills = 8
    mock_game_2.players = {2: mocker.Mock(get_formatted_name=lambda: "Player 2", name_history=["Player 2"])}
    mock_game_2.player_kills = {2: 3}
    mock_game_2.player_self_kills = {2: 2}

    games = [mock_game_1, mock_game_2]

    generator = _report.GroupReportGenerator()
    report = generator.generate_report(games)
    assert generator.get_name() == "GroupReport"
    assert report == {
        "Game 1": {
            "total_kills": 10,
            "players": ["Player 1"],
            "kills": {"Player 1": 5},
            "self-kills": {"Player 1": 1},
            "used_names": {"#1": ["Player 1"]}
        },
        "Game 2": {
            "total_kills": 8,
            "players": ["Player 2"],
            "kills": {"Player 2": 3},
            "self-kills": {"Player 2": 2},
            "used_names": {"#2": ["Player 2"]}
        }
    }

def test_kills_ranking_report(games):

    rep = _report.KillsRankingReportGenerator()

    result = {
            'player_kill_rankings': [{'kills': -7, 'player': 'Isgalamido'}]
        }
    
    assert rep.generate_report(games) == result

def test_kills_means_report(games):

    rep = _report.KillsByMeansReportGenerator()

    result = {
        "kills_by_means": {
            "game_1": {},
            "game_2": {
                "MOD_TRIGGER_HURT": 7,
                "MOD_ROCKET_SPLASH": 3,
                "MOD_FALLING": 1
                            }
        }
    }
    
    assert rep.generate_report(games) == result


def test_json_report_handler_with_mocked_generators(tmp_path, mocker):
    # Mock IReportGenerator
    mock_generator = mocker.Mock(spec=_report.IReportGenerator)
    mock_generator.generate_report.return_value = {"mock_key": "mock_value"}
    mock_generator.get_name.return_value = "MockReport"

    games = [] 
    report_generators = [mock_generator]
    report_path = str(tmp_path)

    handler = _report.JSONReportHandler(report_path, report_generators)
    handler.generate_reports(games)

    file_path = os.path.join(report_path, "MockReport.json")
    assert os.path.exists(file_path)
    
    with open(file_path, 'r') as json_file:
        data = json.load(json_file)
        assert data == {"mock_key": "mock_value"}