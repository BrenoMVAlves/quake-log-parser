# ==================================================================================
# Project: Quake Game Log Parser and Reporter
# Author: Breno Alves
# Date: 2024-03-11
# ==================================================================================


import pytest
from src.game import Game, Player, KillType  # Adjust the import as necessary

# Initialize a fixture for a Game object
@pytest.fixture
def test_game() -> Game:
    game = Game(game_id=1)
    return game

# Test initializing a game
def test_game_initialization(test_game: Game):
    assert test_game.game_id == 1
    assert isinstance(test_game.players, dict)
    assert len(test_game.kills) == 0

def test_game_name(test_game: Game):
    assert test_game.get_name() == "game_1"

# Test adding a new player
def test_add_new_player(test_game: Game):
    test_game.add_player(player_id=1, name="Isgalamido")
    assert len(test_game.players) == 1
    assert test_game.players[1].name == "Isgalamido"

# Test changing a player's name
def test_change_player_name(test_game: Game):
    test_game.add_player(player_id=1, name="Isgalamido")
    test_game.add_player(player_id=1, name="Isgalamido2")
    assert test_game.players[1].name == "Isgalamido2"

# Test recording a kill by another player
def test_record_kill_by_player(test_game: Game):
    test_game.add_player(player_id=1, name="Isgalamido")
    test_game.add_player(player_id=2, name="Mocinha")
    test_game.record_kill(killer_id=1, victim_id=2, kill_type_id=KillType.MOD_SHOTGUN.value)
    assert test_game.total_kills == 1
    assert test_game.player_kills[1] == 1
    assert test_game.player_deaths[2] == 1

# Test recording a self-kill
def test_record_self_kill(test_game: Game):
    test_game.add_player(player_id=1, name="Isgalamido")
    test_game.record_kill(killer_id=1, victim_id=1, kill_type_id=KillType.MOD_ROCKET_SPLASH.value)
    assert test_game.total_kills == 0  # Total kills should not increase
    assert test_game.player_self_kills[1] == 1

# Test recording a kill by the world (environment)
def test_record_kill_by_world(test_game: Game):
    test_game.add_player(player_id=1, name="Isgalamido")
    test_game.record_kill(killer_id=Game.WORLD_ID, victim_id=1, kill_type_id=KillType.MOD_LAVA.value)
    assert test_game.total_kills == 1
    assert test_game.player_kills.get(1) is None or test_game.player_kills[1] == -1
    assert test_game.player_deaths[1] == 1
