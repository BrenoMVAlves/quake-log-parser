# ==================================================================================
# Project: Quake Game Log Parser and Reporter
# Author: Breno Alves
# Date: 2024-03-11
# ==================================================================================


import pytest
from src.kill import KillType, Kill 

class TestKillType:
    def test_from_value_known_value(self):
        assert KillType.from_value(1) == KillType.MOD_SHOTGUN

    def test_from_value_unknown_value(self):
        assert KillType.from_value(100) == KillType.MOD_UNKNOWN

class TestKill:
    def test_kill_initialization(self):
        kill_type = KillType.MOD_SHOTGUN
        killer_id = 1
        victim_id = 2
        kill = Kill(kill_type, killer_id, victim_id)
        
        assert kill.kill_type == kill_type
        assert kill.killer_id == killer_id
        assert kill.victim_id == victim_id


