# ==================================================================================
# Project: Quake Game Log Parser and Reporter
# Author: Breno Alves
# Date: 2024-03-11
# ==================================================================================


import os
import pytest
from src.log_parser import LogParser, Game

CWD = os.path.dirname(__file__)

sample_log_file_path =  os.path.join(CWD, r"sample_data\game1_game2.log")
bad_file =  os.path.join(CWD, r"sample_data\bad_data.log")
print(sample_log_file_path)
@pytest.fixture
def parser():
    return LogParser(sample_log_file_path)

def test_parse_file_not_found():
    parser = LogParser("non_existent_file.log")
    with pytest.raises(FileNotFoundError):
        parser.parse()

def test_parse_file_error():
    parser = LogParser(bad_file)
    with pytest.raises(Exception):
        parser.parse()

def test_parse_initialized_games(parser):
    games = parser.parse()
    assert len(games) == 2

def test_parse_adds_players(parser):
    games = parser.parse()
    game = games[0]
    assert len(game.players) == 1
    assert game.players['2'].player_id == '2'
    assert game.players['2'].name == "Isgalamido"

    game = games[1]
    assert len(game.players) == 2
    assert game.players['2'].player_id == '2'
    assert game.players['2'].name == "Isgalamido"
    assert game.players['3'].player_id == '3'
    assert game.players['3'].name == "Mocinha"
    assert len(game.players['3'].name_history) == 2
    assert game.players['3'].name_history == {'Mocinha', 'Dono da Bola'}


def test_parse_records_kills(parser):
    games = parser.parse()
    
    game = games[0]
    assert len(game.kills) == 0
    assert game.total_kills == 0
    
    game = games[1]
    assert len(game.kills) == 11
    assert game.total_kills == 9
    assert game.player_kills['2'] == -7
    assert game.player_self_kills['2'] == 2


def test_log_parser_initializes_games_correctly(parser):
    games = parser.parse()
    assert len(games) > 0
    assert all(isinstance(game, Game) for game in games)
    assert games[0].game_id == 1 

def test_log_parser_handles_clientuserinfochanged(parser):
    games = parser.parse()
    assert len(games[1].players) > 0  

def test_log_parser_handles_kills(parser):
    games = parser.parse()
    assert games[1].total_kills > 0  # Check that kills were recorded
