import pytest
from src.player import Player

def test_player_initialization():
    player = Player(1, "Isgalamido")
    assert player.player_id == 1
    assert player.name == "Isgalamido"
    assert player.name_history == {"Isgalamido"}

def test_get_formatted_name():
    player = Player(1, "Isgalamido")
    formatted_name = player.get_formatted_name()
    assert formatted_name == "Isgalamido #1"

def test_change_name_updates_name_and_history():
    player = Player(1, "Isgalamido")
    player.change_name("Isgalamido2")
    assert player.name == "Isgalamido2"
    assert player.name_history == {"Isgalamido", "Isgalamido2"}

def test_change_name_to_same_does_not_update_history():
    player = Player(1, "Isgalamido")
    player.change_name("Isgalamido")  
    assert player.name == "Isgalamido"
    assert player.name_history == {"Isgalamido"} 

