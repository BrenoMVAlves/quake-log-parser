# ==================================================================================
# Project: Quake Game Log Parser and Reporter
# Author: Breno Alves
# Date: 2024-03-11
# ==================================================================================

import pytest
from unittest.mock import patch, MagicMock
import os
from src.game_collection import GameCollection, Game, LogParser

CWD = os.path.dirname(__file__)

sample_log_file_path =  os.path.join(CWD, r"sample_data\game1_game2.log")

def test_from_logfile():
   
    game_collection = GameCollection.from_logfile(file_path=sample_log_file_path)

    assert isinstance(game_collection, GameCollection)
    assert len(game_collection.games) == 2

