
# ==================================================================================
# Project: Quake Game Log Parser and Reporter
# Author: Breno Alves
# Date: 2024-03-11
# ==================================================================================

class Player:
    """
    Represents a player in a game, managing their identity and name changes.
    """

    def __init__(self, player_id: str, name:str):
        """
        Initializes a new Player instance with a given player ID and name.
        """
        self.player_id = player_id
        self.name = name
        self.name_history = set([name])
    
    def get_formatted_name(self) -> str:
        """
        Returns the player's name formatted with their player ID.
        """
        return f"{self.name} #{self.player_id}"

    def change_name(self, new_name:str):
        """
        Updates the player's name to a new name if it is different from the current name,
        and adds the new name to the name history.

        """
        if new_name != self.name:
            self.name = new_name
            self.name_history.add(new_name)
