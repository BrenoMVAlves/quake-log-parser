# ==================================================================================
# Project: Quake Game Log Parser and Reporter
# Author: Breno Alves
# Date: 2024-03-11
# ==================================================================================

import os
import json
from abc import ABC, abstractmethod
from typing import List
from collections import defaultdict
from pprint import pprint

# Local application imports
from src.game import Game


class IReportGenerator(ABC):

    @abstractmethod
    def generate_report(self, games: List[Game]) -> dict:
        '''Abstract Method to Game report generation'''
    
    def get_name(self) -> str:
        '''Return Report Name'''

class GroupReportGenerator(IReportGenerator):
    def __init__(self) -> None:
        super().__init__()
        self.name = "GroupReport"
    
    def get_name(self) -> str:
        '''Return Report Name'''
        return self.name

    def generate_report(self, games: List[Game]) -> dict:
        '''Logic to generate and return group reports '''
        report = {}
        for game in games:
            game_report = {
                "total_kills": game.total_kills,
                "players": [player.get_formatted_name() for player_id, player in game.players.items()],
                "kills": {game.players[player_id].get_formatted_name(): count for player_id, count in game.player_kills.items()},
                "self-kills": {game.players[player_id].get_formatted_name(): count for player_id, count in game.player_self_kills.items()},
                "used_names": {f"#{player_id}": list(player.name_history) for player_id, player in game.players.items()}
            }
            report[f"{game.get_name()}"] = game_report
        return report
    
class KillsRankingReportGenerator(IReportGenerator):
    def __init__(self) -> None:
        super().__init__()
        self.name = "KillsRankingReport"
        
    def get_name(self) -> str:
        '''Return Report Name'''
        return self.name
    
    def generate_report(self, games: List[Game]) -> dict:
        player_kills = defaultdict(int)
        for game in games:
            for player_id, kills in game.player_kills.items():
                player_name = game.players[player_id].name
                player_kills[player_name] += kills
        
        # Sort players by kills, descending
        sorted_kills = sorted(player_kills.items(), key=lambda item: item[1], reverse=True)
        
        # Format the report
        report = self.format_ranking_report(sorted_kills)
        return report

    @staticmethod
    def format_ranking_report(sorted_kills):
        '''Formats the kills ranking report for presentation'''
        ranking = [{"player": player, "kills": kills} for player, kills in sorted_kills]
        return {"player_kill_rankings": ranking}

class KillsByMeansReportGenerator(IReportGenerator):
    def __init__(self) -> None:
        super().__init__()
        self.name = "KillsByMeansReport"
    def get_name(self) -> str:
        '''Return Report Name'''
        return self.name

    def generate_report(self, games: List[Game]) -> dict:
        ''' Logic to generate and return kills ranking reports'''
        kills_by_means = {}
        for game in games:
            kills_by_means[game.get_name()] = defaultdict(int)
            for kill in game.kills:
                kill_type_name = kill.kill_type.name
                kills_by_means[game.get_name()][kill_type_name] += 1
            kills_by_means[game.get_name()] = dict(kills_by_means[game.get_name()])
        # Format the report as desired
        report = self.format_report(kills_by_means)
        return report

    @staticmethod
    def format_report(kills_by_means):
        formatted_report = {"kills_by_means": kills_by_means}
        return formatted_report


class JSONReportHandler:
    def __init__(self, report_path: str, report_generators: List[IReportGenerator]) -> None:
        self.report_path = report_path
        self.report_generators = report_generators

    def generate_reports(self, games: List[Game]) -> None:
        """
        Generate reports using the provided report generators and save them to the specified path.
        """
        if not os.path.exists(self.report_path):
            os.makedirs(self.report_path)

        for generator in self.report_generators:
            file_path = os.path.join(self.report_path, generator.get_name() + ".json")
            report = generator.generate_report(games)
            with open(file_path, 'w') as json_file:
                json.dump(report, json_file, indent=4, ensure_ascii=False)

