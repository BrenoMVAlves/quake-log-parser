# ==================================================================================
# Project: Quake Game Log Parser and Reporter
# Author: Breno Alves
# Date: 2024-03-11
# ==================================================================================

from enum import Enum

class KillType(Enum):
    """
    An enumeration representing different types of kills in the game.
    """
    MOD_UNKNOWN = 0
    MOD_SHOTGUN = 1
    MOD_GAUNTLET = 2
    MOD_MACHINEGUN = 3
    MOD_GRENADE = 4
    MOD_GRENADE_SPLASH = 5
    MOD_ROCKET = 6
    MOD_ROCKET_SPLASH = 7
    MOD_PLASMA = 8
    MOD_PLASMA_SPLASH = 9
    MOD_RAILGUN = 10
    MOD_LIGHTNING = 11
    MOD_BFG = 12
    MOD_BFG_SPLASH = 13
    MOD_WATER = 14
    MOD_SLIME = 15
    MOD_LAVA = 16
    MOD_CRUSH = 17
    MOD_TELEFRAG = 18
    MOD_FALLING = 19
    MOD_SUICIDE = 20
    MOD_TARGET_LASER = 21
    MOD_TRIGGER_HURT = 22
    # Uncomment the following if MISSIONPACK is part of the game data
    # MOD_NAIL = 23
    # MOD_CHAINGUN = 24
    # MOD_PROXIMITY_MINE = 25
    # MOD_KAMIKAZE = 26
    # MOD_JUICED = 27
    MOD_GRAPPLE = 28

    @classmethod
    def from_value(cls, value: int) -> Enum:
        """
        Return the KillType enum Member from the Value.
        If the value does not correspond to any known kill type, MOD_UNKNOWN is returned.
        """
        for member in cls:
            if member.value == value:
                return member
        # # Return a default if not found
        return cls.MOD_UNKNOWN 


class Kill:
    """
    Represents a kill event in a game, including the type of kill, the identifier of the killer,
    and the identifier of the victim.
    """
    def __init__(self, kill_type:KillType, killer_id: int, victim_id: int):
        self.kill_type = kill_type
        self.killer_id = killer_id
        self.victim_id = victim_id