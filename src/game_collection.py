# ==================================================================================
# Project: Quake Game Log Parser and Reporter
# Author: Breno Alves
# Date: 2024-03-11
# ==================================================================================

from typing import List

# Local application imports
from src.game import Game
from src.log_parser import LogParser

class GameCollection:
    '''
    Represents a list of Games
    '''
    def __init__(self, games: List[Game]):
        self.games = games

    @classmethod
    def from_logfile(cls, file_path:str):
        '''
        Constructor to generate object from the logfile
        '''
        # Parse the logfile to populate games
        log_parser = LogParser(file_path)
        games = log_parser.parse()
        return cls(games)
