# ==================================================================================
# Project: Quake Game Log Parser and Reporter
# Author: Breno Alves
# Date: 2024-03-11
# ==================================================================================

from typing import List, Dict

# Local application imports
from src.kill import Kill, KillType
from src.player import Player

class Game:
    """
    Represents a single game instance in a Quake game log, tracking players, kills, and game statistics.
    """

    # Constant representing the ID of the "world" entity responsible for environmental kills.
    WORLD_ID = "1022"

    def __init__(self, game_id: int):
        """
        Initializes a new Game instance with the specified game ID.
        """
        self.game_id: int = game_id
        self.players: Dict[str, Player] = {}  
        self.kills: List[Kill] = []
        self.total_kills: int = 0 # Game kills [Include from world, exclue self-kills]

        # Game Helper Maps
        self.player_kills : Dict[str, int] = {} # PlayerID : Player Kills Count
        self.player_deaths : Dict[str, int] = {}  # PlayerID : Player Deaths Count
        self.player_self_kills : Dict[str, int] = {} # PlayerID : Player Self-Kills Count

    def get_name(self) -> str:
        """
        Returns a formatted string representing the game's name, including its ID.
        """
        return f"game_{self.game_id}"
    
    def add_player(self, player_id: str, name: str):
        """
        Adds a new player to the game or updates an existing player's name based on the given player ID and name.
        """
        if player_id in self.players:
            self.players[player_id].change_name(name)
        else:
            self.players[player_id] = Player(player_id, name)


    def record_kill(self, killer_id: str, victim_id: str, kill_type_id: str):
        """"
        Records a kill event in the game, updating game statistics accordingly.
        """
        # Record kill
        kill_type = KillType.from_value(int(kill_type_id))
        self.kills.append(
            Kill(kill_type, killer_id, victim_id)
        )

        # Update Kills
        self.__update_kills(killer_id, victim_id)
    
    
    def __update_kills(self, killer_id: str, victim_id: str):
        """
        [Private] Updates the game's kill and death statistics based on a recorded kill event.
        """

        #Handling self-kills
        if killer_id == victim_id:
            self.player_self_kills[killer_id] = self.player_self_kills.get(killer_id, 0) + 1
            return 
        
        # Incremment Total Kills
        self.total_kills += 1

        # If world kills the player, decrease victim's kill
        if killer_id == self.WORLD_ID: 
            self.player_kills[victim_id] = self.player_kills.get(victim_id, 0) - 1
        else:
            self.player_kills[killer_id] = self.player_kills.get(killer_id, 0) + 1
        self.player_deaths[victim_id] = self.player_deaths.get(victim_id, 0) + 1

