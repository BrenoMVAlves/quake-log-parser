# ==================================================================================
# Project: Quake Game Log Parser and Reporter
# Author: Breno Alves
# Date: 2024-03-11
# ==================================================================================

from typing import List, Tuple

# Local application imports
from src.game import Game

class LogParser:
    '''
    Class for Parsing Quake Game log files 
    '''
    
    def __init__(self, file_path:str):
        '''
        Create a LogParser with a log file specified by the file_path param
        '''
        self.file_path = file_path
    
    def parse(self) -> List[Game]:
        '''
        Parses the Quake game log file specified by the file_path attribute.
        
        This method iterates over each line of the log file, processing it according
        to the type of event it represents. Each "InitGame:" event starts a new Game
        instance, "ClientUserinfoChanged:" events are used to add or update player
        information within the current game, and "Kill:" events record kill actions
        within the game. Other event types can be added as needed.

        Returns:
        List[Game]: A list of Game instances, each representing a game session
        found in the log file. If no games are found or the file cannot be processed,
        an empty list is returned.
        '''
        games = []
        current_game = None
        try:
            with open(self.file_path, 'r') as file:
                for line in file:

                    if "InitGame:" in line:
                        (current_game, games) = self._handle_InitGame(current_game, games)
                        
                    elif "ClientUserinfoChanged:" in line:
                        current_game = self._handle_ClientUserinfoChanged(line, current_game)
                    
                    elif "Kill:" in line:
                        current_game = self._handle_Kill(line, current_game)
                    
                    #TODO Add other types of Entries
        except FileNotFoundError:
            print(f"File not found: {self.file_path}")
            raise
        except PermissionError:
            print(f"Permission denied: {self.file_path}")
            raise
        except Exception as exc:
            print(f"An error occurred while parsing the file: {exc}")
            raise

        if current_game is not None:
            # Add the last game
            games.append(current_game)
        
        return games

    def _handle_InitGame(self, current_game: Game, games: List[Game]) -> Tuple[Game, List[Game]]:
        if current_game is not None:
            # Finish the current game and start a new one
            games.append(current_game)
        current_game = Game( game_id = len(games) + 1)

        return (current_game, games)

    def _handle_ClientUserinfoChanged(self, line:str, current_game: Game) -> Game:

        # Format  20:34 ClientUserinfoChanged: 2 n\Isgalamido\t\0\model\xian/default\hmodel\xian/default\g_redteam\\g_blueteam\\c1\4\c2\5\hc\100\w\0\l\0\tt\0\tl\0
        player_id = line.split(": ")[1].split(" ")[0] 

        player_info_parts = line.split(" n\\")[1] 
        player_name = player_info_parts.split("\\")[0] 

        if current_game:
            current_game.add_player(player_id, player_name)
        
        return current_game

    def _handle_Kill(self, line:str, current_game: Game) -> Game:
        parts = line.split(": ")
        info = parts[1].split(" ")
        killer = info[0]
        killed = info[1]
        kill_type = info[2]
        if current_game:
            current_game.record_kill(killer, killed, kill_type)
        
        return current_game
    
