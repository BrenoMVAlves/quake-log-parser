# quake-log-parser

Quake Log Parser is a Python application designed to parse log files from Quake 3 Arena matches. It extracts valuable game session information, such as game starts, player information, and kill events.

## Requirements

- Python 3.11
- pip (Python package installer)

## Setup and Installation

1. **Clone the Repository**

    First, clone the repository to your local machine using Git:

    ```bash
    git clone https://gitlab.com/BrenoMVAlves/quake-log-parser.git
    
    cd quake-log-parser
    ```

2. **Create a Virtual Environment**

    It's recommended to create a virtual environment to manage the project's dependencies separately from your system-wide Python packages. Run the following command to create a virtual environment:

    ```bash
    python -m venv venv
    ```

    Activate the virtual environment:

    - On Windows:

      ```cmd
      venv\Scripts\activate
      ```

    - On macOS/Linux:

      ```bash
      source venv/bin/activate
      ```

3. **Install Dependencies**

    With the virtual environment activated, install the project dependencies using:

    ```bash
    pip install -r requirements.txt
    ```

## Running the Application

To run the Quake Log Parser, execute the following command from the root of the project directory:

```bash
python quake_log_parser.py
```
## Reports

After Run the application a **Reports** Folder will be created on the root of the package.
It contains the following JSON Report files:

- **GroupReport.json**: Report of Game data grouped by Game Session.
- **KillsRankingReport.json**: Report of Kills Ranking by Player Name.
- **KillsByMeansReport.json**: Report of Kill data grouped by Game Session.

## Assumptions and Implementation Details

To ensure clarity in how the Quake Log Parser operates and how reports are generated, the following assumptions have been made in the code:

- **Player Names**: During some games the same Session ID is given to different player names. On the code we assume that the Name can be changed by the Player and so the Player Data is grouped by the Session ID. Also we assume different Players can use the same Player Name on a Game Session.

- **Name Changes During a Match**: If a player changes their name during a match, the report will count the kills by the Player Session ID and display the last name used for that player within the session.

- **Negative Number of Kills**: Players can have a negative kill count. This occurs when a player's deaths caused by the <world> (`<world>`).

- **Self-kills and Scoring**: Kills where a player causes their own death do not count towards the `total_kills`. Furthermore, self-kills are not added to the player's kill count in reports.

- **Penalty for Deaths by `<world>`**: When a player is killed by `<world>` they lose one point from their kill score.

- **Exclusion of `<world>` from Player Lists and Kill Dictionaries**: Since `<world>` is not an actual player, it does not appear in the list of players or in the kill dictionaries within generated reports.

- **Total Kills Count**: The `total_kills` counter encompasses all kills, including the ones from the `<world>`.

## Running Tests and Measuring Coverage
This project uses pytest for testing. To run tests, navigate to the project's root directory and execute:

```bash
pytest
```

For test coverage, ensure you have pytest-cov installed. 

```bash
pytest --cov=src tests/
```
