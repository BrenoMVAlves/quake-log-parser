# ==================================================================================
# Project: Quake Game Log Parser and Reporter
# Author: Breno Alves
# Date: 2024-03-11
# ==================================================================================

import os
from src.game_collection import GameCollection
import src.report as report

CWD = os.path.dirname(__file__)

REPORT_PATH = os.path.join(CWD, "Reports")

LOG_FILE = os.path.join(CWD, r"log_samples\qgames.log")

match_handler = GameCollection.from_logfile(LOG_FILE)

reports_gen = [
    report.GroupReportGenerator(),
    report.KillsRankingReportGenerator(),
    report.KillsByMeansReportGenerator(),
]

# Generate JSON Report
reporter = report.JSONReportHandler(
    report_path=REPORT_PATH,
    report_generators=reports_gen
)

reporter.generate_reports(match_handler.games)
